import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

const App = () => {
  
  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName ] = useState("");

  return (
    <View>

      <Text style={styles.title} > Nosso Olá Mundo! </Text>

      <TextInput 
        value = {firstName}
        onChangeText= {setFirstName}
        style={styles.input}
        placeholder="First and Middle Name"
      />

      <TextInput 
        value= {lastName}
        onChangeText= {setLastName}
        style= {styles.input}
        placeholder="Last Name"
      />

      <Text style={styles.text}> Hi {lastName}, {firstName}! Welcome!!! </Text>

    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    textAlign: 'center',
    marginTop: 32,
    marginBottom: 20
  },

  input: {
    fontSize: 16,
    borderWidth: 1,
    margin: 5,
    borderRadius: 15
  },

  text: {
    fontSize: 16,
    margin: 5
  }
});

export default App;